public class DbConnectionDetailsBuilder {
    private String url;
    private String username;
    private String password;

    public DbConnectionDetailsBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public DbConnectionDetailsBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public DbConnectionDetailsBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public DbConnectionDetails build() {
        return new DbConnectionDetails(url, username, password);
    }
}