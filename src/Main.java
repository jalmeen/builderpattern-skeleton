public class Main {

    public static void main(String[] args) {

        DbConnectionDetails dbConnectionDetails = new DbConnectionDetailsBuilder()
                .setUsername("Murray")
                .setUrl("mongodb://db1.test.com:27017")
                .setPassword("Qwerty@321")
                .build();

        System.out.println(dbConnectionDetails);
    }
}
